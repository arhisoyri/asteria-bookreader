import { app, BrowserWindow, dialog, ipcMain } from "electron";
import { App } from "./App";
import { TxtFile } from "./FileTypes/TxtFile";

export class FileSystem {
  private isOpened: boolean;

  constructor() {
    this.isOpened = false;
  }

  registerHandlers() {
    ipcMain.handle("openFileDialog", (event) => {
      this.openFileDialog();
    });
  }

  async openFileDialog() {
    // open filesystem dialog to choose files
    if (this.isOpened) return;
    //TODO Should try to make openfilsystem window go on top if already opened...Modal? Model?

    this.isOpened = true;
    const files = await dialog.showOpenDialog({
      properties: ["openFile"],
      //Should consider adding not ony Cyrillic but also Math
      filters: [{ name: "Text Files", extensions: ["txt"] }],
    });
    this.isOpened = false;

    if (files.canceled) return;
    const path = files.filePaths[0];
    //Create instance here and pass him the  path
    this.openFile(path);
  }

  public openFile(path: string) {
    const txtFile = new TxtFile(path);
    App.getInstance().pageData.openFile(txtFile);
  }
}
