import { FileSystem } from "./FileSystem";
import { BookData } from "./BookData";
import { BrowserWindow } from "electron";
import * as path from "path";
import * as url from "url";

export class App {
  public fileSystem: FileSystem;
  public pageData: BookData;
  public mainWindow: BrowserWindow | null;
  private static instance: App;

  private constructor() {
    this.fileSystem = new FileSystem();
    this.pageData = new BookData();
    this.mainWindow = null;
  }

  public static getInstance(): App {
    if (!App.instance) {
      App.instance = new App();
    }
    return App.instance;
  }

  public init() {
    this.fileSystem.registerHandlers();
    this.pageData.registerHandlers();
    //load initial/saved Settings
    //load initial/saved State Not showing it yet!
  }

  // open a window
  public createWindow() {
    this.mainWindow = new BrowserWindow({
      width: 1100,
      height: 700,
      show: false,
      //frame: false,
      icon: path.join(__dirname, "../icons/png/256x256.png"),
      //Should put it into build
      backgroundColor: "#010620",
      webPreferences: {
        nodeIntegration: false,
        contextIsolation: false,
        preload: path.join(__dirname, "preload.js"),
      },
    });
    this.mainWindow.once("ready-to-show", () => {
      this.mainWindow.show();
    });
    this.mainWindow.setMenuBarVisibility(false);
    this.mainWindow.loadURL(
      process.env.ELECTRON_START_URL ||
        url.format({
          pathname: path.join(__dirname, "../index.html"),
          protocol: "file:",
          slashes: true,
        })
    );

    // when all windows are closed, quit the app
    this.mainWindow.on("closed", () => {
      this.mainWindow = null;
    });
  }

  startApp() {
    //takes lastOpened from State
    //if not null =>  FileSystem.openFile()
  }
}
