export interface IFileType {
  filePath: string;
  readTxtFile(): Promise<string | undefined>;
  readBookName(): Promise<string | undefined>;
}
