import { app } from "electron";
import { App } from "./App";

App.getInstance().init();
app.on("ready", () => App.getInstance().createWindow());

app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.allowRendererProcessReuse = true;
