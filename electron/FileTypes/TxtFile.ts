import * as fs from "fs";
import { decode } from "iconv-lite";
import { IFileType } from "IFileType";
import { detect } from "jschardet";
import path = require("path");

export class TxtFile implements IFileType {
  public filePath: string;

  constructor(path: string) {
    this.filePath = path;
  }

  async readBookName(): Promise<string> {
    const bookname = path.basename(this.filePath, ".txt");
    return bookname;
  }

  async readTxtFile(): Promise<string | undefined> {
    let dataBuffer: Buffer;
    dataBuffer = await fs.promises.readFile(this.filePath, {});
    let secondaryBuffer: Buffer;
    if (dataBuffer.byteLength >= 10000) {
      secondaryBuffer = dataBuffer.slice(0, 10000);
    } else {
      secondaryBuffer = dataBuffer;
    }

    const encoding = detect(secondaryBuffer);
    let data: string;
    if (encoding.encoding == null) {
      throw new Error(
        `The File ${path.basename(
          this.filePath
        )} is corrupt and therefore cannot be opened`
      );
    }
    data = decode(dataBuffer, encoding.encoding);

    return data;
  }
}
