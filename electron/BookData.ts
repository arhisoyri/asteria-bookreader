import { webContents } from "electron";
import { App } from "./App";
import { TxtFile } from "./FileTypes/TxtFile";
import { IBookData } from "../src/@types/IBookData";

export class BookData {
  txtFile?: TxtFile;

  registerHandlers() {}
  /// tell renerer: here is the title, here is the text
  async openFile(txtFile: TxtFile) {
    const contents = App.getInstance().mainWindow?.webContents;
    if (contents == null) {
      console.log("Main window has not initialized");
      return;
    }
    let bookData: IBookData;
    try {
      //ask for content of file in textFile
      const dataFromTxt = await txtFile.readTxtFile();
      const bookNameTxt = await txtFile.readBookName();

      bookData = {
        bookContent: dataFromTxt,
        bookName: bookNameTxt,
        success: true,
      };
    } catch (e) {
      bookData = {
        error: e.message,
        success: false,
      };
    }
    contents.send("fileContent", bookData);
  }
  getPageCount() {}
  pageWasSwitched() {}
}
