export interface IBookData {
  bookName?: string;
  bookContent?: string;
  pageCount?: number;
  success: boolean;
  error?: string;
}
