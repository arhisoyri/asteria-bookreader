import { RoutesEnum } from "../enums/routesEnum";

export interface ILocationUpdate {
  locationUpdated(location: RoutesEnum): void;
}
