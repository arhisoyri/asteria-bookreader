import { createTheme } from "@material-ui/core/styles";

export const theme = createTheme({
  breakpoints: {
    values: {
      xs: 0,
      sm: 0,
      md: 960,
      lg: 1280,
      xl: 1920,
    },
  },
  palette: {
    primary: {
      main: "#01082b",
    },
    secondary: {
      main: "#3a5688",
    },
    background: {
      paper: "#191d35",
      default: "#010620",
    },
    text: {
      primary: "#a2a5b4",
    },
    action: {
      active: "#3a5688",
      //hover: '#5b6079',
      selected: "#5b60796",
      disabled: "#141724",
    },
  },

  overrides: {
    MuiCssBaseline: {
      "@global": {
        "*": {
          "scrollbar-width": "thin",
        },
        "*::-webkit-scrollbar": {
          width: "18px",
          backgroundColor: "#131628",
        },
        "*::-webkit-scrollbar-corner": {
          backgroundColor: "#131628",
        },
        "*::-webkit-scrollbar-thumb": {
          backgroundColor: "#36486a",
          borderRadius: "3px",
          //outline: '1px solid slategrey',
        },
      },
    },

    // Style sheet name ⚛️
    MuiDrawer: {
      // Name of the rule
      paper: {
        // Some CSS
        position: "relative",
      },
    },
    MuiAppBar: {
      root: {
        position: "relative",
      },
    },
  },

  typography: {
    h4: {
      color: "#616e7e",
    },
  },
});
