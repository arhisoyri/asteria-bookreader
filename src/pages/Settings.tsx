import { withStyles, WithStyles } from "@material-ui/core";
import { styles } from "./SettingsStyle";

export const Settings = (props: WithStyles<typeof styles, false>) => {
  const { classes } = props;
  //const vm = this.props.model;
  return <div className={classes.wholePage}></div>;
};

export const SettingsWithStyles = withStyles(styles)(Settings);
