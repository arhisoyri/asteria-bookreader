import { createStyles, Theme } from "@material-ui/core/styles";

export const styles = (theme: Theme) =>
  createStyles({
    content: {
      padding: theme.spacing(3),
      transition: theme.transitions.create("margin", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),

      marginLeft: 0,
    },

    contentShift: {
      transition: theme.transitions.create("margin", {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    // page: {
    //   gridArea: "page",
    //   height: "100%",
    //   minWidth: "0",
    //   minHeight: "0",
    // },

    wholePage: {
      overflowY: "auto",
      height: "100%",

      position: "relative",
    },
    image: {
      display: "block",
      opacity: "15%",
      maxHeight: "100%",
      maxWidth: "100%",
      margin: "auto",
      // height: "100%",
      // width: "100%",
      objectFit: "cover",
      // filter: "invert(70%)",
    },
  });
