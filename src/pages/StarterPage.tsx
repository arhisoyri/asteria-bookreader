import { withStyles, WithStyles } from "@material-ui/core";
import { styles } from "./StarterPageStyle";
import StarWheel from "../assets/moon1.svg";

export const StarterPage = (props: WithStyles<typeof styles, false>) => {
  const { classes } = props;
  // const vm = new PageStore();
  //I need to re-render component if model is updated...
  //const vm =
  return (
    <div>
      <div className={classes.wholePage}>
        <img src={StarWheel} className={classes.image} />
      </div>
    </div>
  );
};

export const StarterPageWithStyles = withStyles(styles)(StarterPage);
