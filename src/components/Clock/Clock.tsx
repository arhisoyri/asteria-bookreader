import { Box, withStyles, WithStyles } from "@material-ui/core";
import { observer } from "mobx-react";
import React from "react";
import { ClockStore } from "../../stores/ClockStore";
import { styles } from "./ClockStyle";

export interface ClockProps extends WithStyles<typeof styles, true> {
  model: ClockStore;
}

// export interface SideBarProps extends WithStyles<typeof styles, true> {
//   model: SideBarStore;
// }

@observer
class Clock extends React.Component<ClockProps> {
  public render(): React.ReactNode {
    const vm = this.props.model;
    const { classes } = this.props;
    return (
      <Box className={classes.clockStyle} justifyContent="flex-end">
        {vm.clockAppearance()}
      </Box>
    );
  }
}

export default withStyles(styles, { withTheme: true })(Clock);
