import { createStyles } from "@material-ui/core/styles";

export const styles = () =>
  createStyles({
    clockStyle: {
      fontSize: "medium",
      color: "#71738c",
      position: "absolute",
      right: "2%",
      top: "15%",
      fontWeight: 700,
    },
  });
