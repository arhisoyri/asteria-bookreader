import {
  ListItem,
  ListItemIcon,
  ListItemText,
  withStyles,
  WithStyles,
} from "@material-ui/core";
import { observer } from "mobx-react";
import React from "react";
import { styles } from "./SideBarButtonStyle";

export interface SideBarButtonsProps extends WithStyles<typeof styles, true> {
  //parameters here
  icon: JSX.Element;
  title: string;
  onButtonPress?: () => void;
}

@observer
class SideBarButton extends React.Component<SideBarButtonsProps> {
  public render(): React.ReactNode {
    return (
      <ListItem
        onClick={() => {
          if (this.props.onButtonPress) this.props.onButtonPress();
        }}
        button
        key={this.props.title}
      >
        <ListItemIcon>{this.props.icon}</ListItemIcon>
        <ListItemText primary={this.props.title} />
      </ListItem>
    );
  }
}

export default withStyles(styles, { withTheme: true })(SideBarButton);
