import { createStyles, Theme } from "@material-ui/core/styles";

export const styles = (theme: Theme) =>
  createStyles({
    footer: {
      height: "100%",
      gridArea: "footer",
      backgroundColor: "#1a1e36",
    },
    textStyle: {
      fontSize: "small",
      color: "#71738c",
      position: "relative",
      marginLeft: "50%",
      fontWeight: 720,
    },
  });
