import { withStyles, WithStyles } from "@material-ui/core";
import Divider from "@material-ui/core/Divider";
import { observer } from "mobx-react";
import React from "react";
import { PageStore } from "../../stores/PageStore";
import { styles } from "./FooterStyle";

export interface FooterProps extends WithStyles<typeof styles, true> {
  model: PageStore;
}

@observer
class Footer extends React.Component<FooterProps> {
  public render(): React.ReactNode {
    //const vm = this.props.model;
    const { classes } = this.props;

    return (
      <div className={classes.footer}>
        <Divider />
        <div className={classes.textStyle}>6/666</div>
      </div>
    );
  }
}

export default withStyles(styles, { withTheme: true })(Footer);
