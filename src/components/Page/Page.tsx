import {
  Box,
  CssBaseline,
  Typography,
  withStyles,
  WithStyles,
} from "@material-ui/core";
import clsx from "clsx";
import { observer } from "mobx-react";
import React from "react";
import { PageStore } from "../../stores/PageStore";
import { styles } from "./PageStyle";

export interface PageProps extends WithStyles<typeof styles, true> {
  model: PageStore;
}

@observer
class Page extends React.Component<PageProps> {
  componentDidMount() {
    this.props.model.resizeObserve();
  }
  public render(): React.ReactNode {
    const vm = this.props.model;
    const { classes } = this.props;

    return (
      <div
        ref={vm.scrollDiv}
        onScroll={(e) => vm.handleScroll(e)}
        className={classes.scrollbar}
      >
        <CssBaseline />
        <main>
          <Box
            className={clsx(classes.content, {
              // [classes.contentShift]: vm.root.open,
            })}
          >
            <Typography paragraph className={classes.typography}>
              {vm.text}
            </Typography>
          </Box>
        </main>
      </div>
    );
  }
}

export default withStyles(styles, { withTheme: true })(Page);
