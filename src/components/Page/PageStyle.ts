import { createStyles, Theme } from "@material-ui/core/styles";

export const styles = (theme: Theme) =>
  createStyles({
    typography: {
      whiteSpace: "pre-wrap",
      color: "#c4c8d8",
    },

    content: {
      padding: theme.spacing(3),
      transition: theme.transitions.create("margin", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),

      marginLeft: 0,
    },

    scrollbar: {
      overflowY: "auto",
      position: "absolute",
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
    },

    contentShift: {
      transition: theme.transitions.create("margin", {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    // page: {
    //   gridArea: "page",
    //   height: "100%",
    //   minWidth: "0",
    //   minHeight: "0",
    // },
  });
