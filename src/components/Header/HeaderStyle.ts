import { createStyles, Theme } from "@material-ui/core/styles";

export const styles = (theme: Theme) =>
  createStyles({
    typography: {
      color: "#8488aa",
    },
    appBar: {
      boxShadow: "none",
      background: "#14172c",
      transition: theme.transitions.create(["margin", "width"], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
    },

    counterStyle: {
      fontSize: "medium",
      color: "#606175",
      position: "absolute",
      fontWeight: 700,
      right: "2%",
      bottom: "15%",
    },

    appBarShift: {
      transition: theme.transitions.create(["margin", "width"], {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    menuButton: {
      marginRight: theme.spacing(2),
      color: "#5785c1",
    },
    hide: {
      display: "none",
    },

    firstColumnShow: {
      gridColumn: "1",
      display: "block",
    },
    firstColumnHide: {
      gridColumn: "1",
      display: "none",
    },

    header: {
      height: "100%",
      gridArea: "header",
    },

    show: {
      display: "block",
    },
  });
