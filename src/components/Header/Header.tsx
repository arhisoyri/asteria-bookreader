import React from "react";
import clsx from "clsx";
import {
  AppBar,
  CssBaseline,
  Grid,
  IconButton,
  Toolbar,
  Typography,
  withStyles,
  WithStyles,
} from "@material-ui/core";
import { observer } from "mobx-react";
import { HeaderStore } from "../../stores/HeaderStore";
import Clock from "../Clock/Clock";
import { styles } from "./HeaderStyle";
import MenuIcon from "@material-ui/icons/Menu";
import { RoutesEnum } from "../../enums/routesEnum";

export interface HeaderProps extends WithStyles<typeof styles, true> {
  model: HeaderStore;
}

@observer
class Header extends React.Component<HeaderProps> {
  public render(): React.ReactNode {
    const vm = this.props.model;
    const { classes } = this.props;
    return (
      <div
        className={clsx(this.props.classes.header, {
          [classes.appBarShift]: vm.root.open,
        })}
      >
        <CssBaseline />
        <AppBar position="relative" className={classes.appBar}>
          <Toolbar>
            {this.renderHeaderIconButton()}
            {this.renderHeaderContent()}
          </Toolbar>
        </AppBar>
      </div>
    );
  }

  private renderHeaderContent() {
    const vm = this.props.model;
    let headerTitle = "";
    let currentPages = "";
    let totalPages = "";
    let pages = "";

    switch (vm.navLocation) {
      case RoutesEnum.Settings: {
        headerTitle = "Settings";
        break;
      }
      case RoutesEnum.StarterPage: {
        headerTitle = "";
        break;
      }
      case RoutesEnum.Page: {
        if (!vm.root.pageStore.title) {
          headerTitle = "";
        } else {
          headerTitle = vm.root.pageStore.title;
          currentPages =
            vm.currentPage !== undefined ? vm.currentPage.toString() : "";
          totalPages =
            vm.totalPages !== undefined ? vm.totalPages.toString() : "";
          pages = ` ${currentPages} / ${totalPages}`;
        }
        break;
      }
    }
    return (
      <Grid
        container
        direction="row"
        justifyContent="space-between"
        alignItems="center"
      >
        <Typography className={this.props.classes.typography} variant="h6">
          {headerTitle}
        </Typography>
        <Clock model={this.props.model.root.clockStore} />

        <div className={this.props.classes.counterStyle}>{pages}</div>
      </Grid>
    );
  }

  private renderHeaderIconButton() {
    const vm = this.props.model;

    return (
      <IconButton
        color="inherit"
        aria-label="open drawer"
        onClick={() => vm.root.handleDrawerOpen()}
        edge="start"
        className={clsx(
          this.props.classes.menuButton,
          vm.root.open && this.props.classes.firstColumnHide
        )}
      >
        <MenuIcon />
      </IconButton>
    );
  }
}

export default withStyles(styles, { withTheme: true })(Header);
