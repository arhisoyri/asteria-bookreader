import { createStyles, Theme } from "@material-ui/core/styles";

export const styles = (theme: Theme) =>
  createStyles({
    background: {
      position: "fixed",
      left: "0px",
      right: "0px",
      top: "0px",
      bottom: "0px",
      zIndex: 3000,
      backgroundColor: "rgb(14 14 17 / 40%)",
      //visibility: "hidden",
    },
    buttonPosition: {
      position: "absolute",
      left: "50%",
      top: "50%",
    },

    button: {
      backgroundColor: "#3a5688",
    },
  });
