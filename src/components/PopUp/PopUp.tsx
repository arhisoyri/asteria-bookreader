import { Button, withStyles, WithStyles } from "@material-ui/core";

import { observer } from "mobx-react";
import React from "react";
import { PopUpStore } from "../../stores/PopUpStore";
import { styles } from "./PopUpStyle";

export interface PopUpProps extends WithStyles<typeof styles, true> {
  model: PopUpStore;
}

@observer
class PopUp extends React.Component<PopUpProps> {
  public render(): React.ReactNode {
    const vm = this.props.model;
    const { classes } = this.props;
    if (vm.error != null)
      return (
        <div className={classes.background}>
          <div className={classes.buttonPosition}>
            <div>{vm.error}</div>
            <Button className={classes.button} onClick={() => vm.clear()}>
              Ok
            </Button>
          </div>
        </div>
      );
    else return <></>;
  }
}

export default withStyles(styles, { withTheme: true })(PopUp);
