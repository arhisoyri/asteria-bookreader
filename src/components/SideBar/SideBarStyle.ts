import { createStyles, Theme } from "@material-ui/core/styles";

export const styles = (theme: Theme) =>
  createStyles({
    hide: {
      display: "none",
    },
    drawer: {
      height: "100%",
      flexShrink: 0,
    },

    firstColumnHide: {
      gridColumn: "1",
      display: "none",
    },

    drawerHeader: {
      height: "64px",
      background: "#14172c",
      // alignItems: "right",
      // verticalAlign: "middle",
      padding: theme.spacing(0, 1),
      // necessary for content to be below app bar
      // ...theme.mixins.toolbar,
      justifyContent: "flex-end",
      display: "flex",
      /* justify-content: center; */
      // justifyContent: right;
      alignItems: "center",
    },

    sidebar: {
      gridArea: "sidebar",
      minWidth: "250px",
    },

    content: {
      padding: theme.spacing(0),
      transition: theme.transitions.create("margin", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
    },

    contentShift: {
      transition: theme.transitions.create("margin", {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
      marginLeft: 0,
    },
    link: {
      textDecoration: "none",
      color: "#a2a5b4",
    },
  });
