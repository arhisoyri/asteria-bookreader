import React from "react";
import clsx from "clsx";
import Drawer from "@material-ui/core/Drawer";
//import CssBaseline from "@material-ui/core/CssBaseline";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import { styles } from "./SideBarStyle";
import { CssBaseline, WithStyles, withStyles } from "@material-ui/core";
import { Link } from "react-router-dom";
import { SideBarStore } from "../../stores/SideBarStore";
import { RoutesEnum } from "../../enums/routesEnum";
import { observer } from "mobx-react";
import {
  Brightness1TwoTone,
  Brightness4TwoTone,
  ChromeReaderModeTwoTone,
  ComputerTwoTone,
  SettingsTwoTone,
} from "@material-ui/icons";
import SideBarButton from "../SideBarButton/SideBarButton";

export interface SideBarProps extends WithStyles<typeof styles, true> {
  model: SideBarStore;
}
@observer
class SideBar extends React.Component<SideBarProps> {
  public render(): React.ReactNode {
    const vm = this.props.model;
    const { classes } = this.props;
    return (
      <div
        className={clsx(
          this.props.classes.sidebar,
          !vm.root.open && classes.firstColumnHide,
          {
            [classes.content]: vm.root.open,
          }
        )}
      >
        <CssBaseline />
        <Drawer
          variant="persistent"
          anchor="left"
          open={vm.root.open}
          classes={{}}
          className={classes.drawer}
        >
          <div className={classes.drawerHeader}>
            <IconButton onClick={() => vm.root.handleDrawerClose()}>
              <ChevronLeftIcon />
            </IconButton>
          </div>

          <Divider />
          <List>
            <Link to={RoutesEnum.Page} className={classes.link}>
              <SideBarButton
                icon={<ChromeReaderModeTwoTone />}
                title={"Book"}
              />
            </Link>
          </List>
          <Divider />
          <List>
            <SideBarButton
              icon={<ComputerTwoTone />}
              title={"Files"}
              onButtonPress={() => vm.root.bookStore.openFile()}
            />
          </List>

          <Divider />
          <List>
            <SideBarButton icon={<Brightness4TwoTone />} title={"Dark Theme"} />
            <SideBarButton
              icon={<Brightness1TwoTone />}
              title={"Light Theme"}
            />
          </List>
          <Divider />

          <List>
            <Link to={RoutesEnum.Settings} className={classes.link}>
              <SideBarButton icon={<SettingsTwoTone />} title={"Settings"} />
            </Link>
          </List>
        </Drawer>
      </div>
    );
  }
}

export default withStyles(styles, { withTheme: true })(SideBar);
