import { LocationState, History, Location } from "history";
import { makeObservable, observable } from "mobx";
import { RoutesEnum } from "../../src/enums/routesEnum";
import { ILocationUpdate } from "../@types/ILocationUpdate";

export class RouterService {
  private history?: History<LocationState>;
  private location?: Location<LocationState>;
  private navLocation: RoutesEnum | null = null;
  private subscribers: ILocationUpdate[] = [];

  public init(
    history: History<LocationState>,
    location: Location<LocationState>
  ) {
    this.history = history;
    this.location = location;
  }

  subscribe(s: ILocationUpdate) {
    this.subscribers.push(s);
  }

  notifySubscribers() {
    this.subscribers.forEach((s: ILocationUpdate) => {
      if (!this.navLocation) {
        throw new Error("this.navLocation from RouterService is null");
      }
      s.locationUpdated(this.navLocation);
    });
  }
  navigateToPage() {
    if (!this.history) {
      throw new Error("this.history from RouterService is undefined");
    }
    this.history.push(RoutesEnum.Page);
  }

  public listeningToLocation() {
    if (!this.location) {
      throw new Error("this.location from RouterService is undefined");
    }
    switch (this.location.pathname) {
      case RoutesEnum.Settings: {
        this.navLocation = RoutesEnum.Settings;
        break;
      }
      case RoutesEnum.Page: {
        this.navLocation = RoutesEnum.Page;
        break;
      }
      default:
        this.navLocation = RoutesEnum.StarterPage;
        break;
    }

    this.notifySubscribers();
  }
}
