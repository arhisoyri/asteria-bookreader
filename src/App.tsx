import { theme } from "./styles/GlobalStyle";
import SideBar from "./components/SideBar/SideBar";
import {
  CssBaseline,
  MuiThemeProvider,
  withStyles,
  WithStyles,
} from "@material-ui/core";

import { RootStore } from "./stores/RootStore";
import Header from "./components/Header/Header";
import Page from "./components/Page/Page";
import PopUp from "./components/PopUp/PopUp";
import { styles } from "./AppStyle";
import { SettingsWithStyles } from "./pages/Settings";
import { StarterPageWithStyles } from "./pages/StarterPage";
import { Switch, Route } from "react-router-dom";
import { RoutesEnum } from "./enums/routesEnum";
import { useHistory, useLocation } from "react-router-dom";
import { useEffect } from "react";
//import Footer from "./components/Footer/Footer";

const root = new RootStore();

const App = (props: WithStyles<typeof styles, false>) => {
  const { classes } = props;
  //const classes = props.classes;
  //make it nnot nested? And export?
  //TODO Transform
  const history = useHistory();
  const location = useLocation();
  root.routerService.init(history, location);
  useEffect(() => {
    root.routerService.listeningToLocation();
  });

  return (
    <div className={classes.wrapper}>
      <MuiThemeProvider theme={theme}>
        <CssBaseline />
        <PopUp model={root.popUpStore} />
        <Header model={root.headerStore} />
        <SideBar model={root.sideBarStore} />
        <div className={classes.gridPage}>
          <Switch>
            <Route
              path={RoutesEnum.Page}
              render={() => <Page model={root.pageStore} />}
            />
            <Route path={RoutesEnum.Settings} component={SettingsWithStyles} />
            <Route
              path={RoutesEnum.StarterPage}
              exact
              component={StarterPageWithStyles}
            />
          </Switch>
        </div>
        {/* <Footer model={root.pageStore} /> */}
      </MuiThemeProvider>
    </div>
  );
};

export const AppWithStyles = withStyles(styles)(App);
