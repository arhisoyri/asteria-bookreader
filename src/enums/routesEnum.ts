export enum RoutesEnum {
  StarterPage = "/",
  Settings = "/settings",
  Page = "/page",
}
