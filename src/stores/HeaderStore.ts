import { computed, makeObservable, observable } from "mobx";
import { ILocationUpdate } from "../@types/ILocationUpdate";
import { RoutesEnum } from "../enums/routesEnum";
import { RootStore } from "./RootStore";

export class HeaderStore implements ILocationUpdate {
  root: RootStore;
  navLocation: RoutesEnum | null = null;
  constructor(root: RootStore) {
    this.root = root;

    makeObservable(this, {
      totalPages: computed,
      currentPage: computed,
      navLocation: observable,
    });
    this.root.routerService.subscribe(this);
  }
  locationUpdated(navLocation: RoutesEnum) {
    this.navLocation = navLocation;
  }

  public get totalPages(): number {
    return Math.round(
      this.root.pageStore.endOfTheBook / this.root.pageStore.pageHeight
    );
  }

  public get currentPage(): number {
    return (
      Math.round(
        this.root.pageStore.placeInTheBook / this.root.pageStore.pageHeight
      ) + 1
    );
  }
}
