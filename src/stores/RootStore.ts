import { BookStore } from "./BookStore";
import { SideBarStore } from "./SideBarStore";
import { PageStore } from "./PageStore";
import { HeaderStore } from "./HeaderStore";
import { ClockStore } from "./ClockStore";

import { action, makeObservable, observable } from "mobx";
import { RouterService } from "../services/RouterService";
import { PopUpStore } from "./PopUpStore";
export class RootStore {
  popUpStore: PopUpStore;
  bookStore: BookStore;
  sideBarStore: SideBarStore;
  pageStore: PageStore;
  headerStore: HeaderStore;
  clockStore: ClockStore;
  open: boolean = false;
  routerService: RouterService;

  constructor() {
    this.routerService = new RouterService();
    this.popUpStore = new PopUpStore(this);
    this.bookStore = new BookStore(this);
    this.sideBarStore = new SideBarStore(this);
    this.pageStore = new PageStore(this);
    this.headerStore = new HeaderStore(this);
    this.clockStore = new ClockStore(this);

    makeObservable(this, {
      open: observable,
      handleDrawerOpen: action,
      handleDrawerClose: action,
    });
  }

  // Take it to the root
  handleDrawerOpen(): void {
    // setOpen(true);
    //this is action for mobx
    //this is a function from React library, akin to useState
    this.open = true;
  }

  handleDrawerClose(): void {
    //setOpen(false);
    //this is action for mobx
    //this is a function from React library, akin to useState
    this.open = false;
  }

  //Sozdat object classa pop-up(write class)
  //with method showerror
}
