import { action, makeObservable, observable } from "mobx";
import React, { RefObject } from "react";
import { IBookData } from "../@types/IBookData";
import { ILocationUpdate } from "../@types/ILocationUpdate";
import { RootStore } from "./RootStore";

export class PageStore implements ILocationUpdate {
  root: RootStore;
  title?: string;
  text?: string;
  placeInTheBook: number = 0;
  endOfTheBook: number = 0;
  pageHeight: number = 0;
  scrollDiv: RefObject<HTMLDivElement> = React.createRef();
  //routerservice: RouterService;

  constructor(root: RootStore) {
    this.root = root;
    //this.root.routerService.navLocation;

    makeObservable(this, {
      text: observable,
      title: observable,
      placeInTheBook: observable,
      endOfTheBook: observable,
      pageHeight: observable,
      updateBook: action,
      saveDimenshions: action,
    });

    window.ipcRenderer.on("fileContent", (_event, dataFromTxt: IBookData) => {
      if (!dataFromTxt.success) {
        root.popUpStore.showError(dataFromTxt.error ?? "");
        return;
      }

      this.updateBook(
        dataFromTxt.bookContent ?? "",
        dataFromTxt.bookName ?? ""
      );
      this.root.routerService.navigateToPage();

      this.scrollDiv.current?.scrollTo(0, 0);
      if (this.scrollDiv.current != undefined) {
        this.saveDimenshions(this.scrollDiv.current);
      }
    });

    this.root.routerService.subscribe(this);
  }
  locationUpdated(): void {
    this.scrollDiv.current?.scrollTo(0, this.placeInTheBook);
  }
  resizeObserve() {
    const resizeObserver = new ResizeObserver(
      (entry: ResizeObserverEntry[]) => {
        if (entry.length > 1) {
          throw "Page is obserwing more than one div";
        }
        this.saveDimenshions(entry[0].target as HTMLDivElement);
      }
    );
    if (this.scrollDiv.current != undefined) {
      resizeObserver.observe(this.scrollDiv.current);
    }
  }

  public updateBook(text: string, title: string): void {
    this.text = text;
    this.title = title;
  }

  saveDimenshions(element: HTMLDivElement) {
    this.placeInTheBook = element.scrollTop;
    this.endOfTheBook = element.scrollHeight;
    this.pageHeight = element.offsetHeight;
  }

  public handleScroll(e: React.UIEvent<HTMLDivElement, UIEvent>) {
    let element = e.target as HTMLDivElement;
    this.saveDimenshions(element);
  }
}
