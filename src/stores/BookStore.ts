import { RootStore } from "./RootStore";
//Merge with PageStore

export class BookStore {
  root: RootStore;
  path: String | null;
  constructor(root: RootStore) {
    this.root = root;
    this.path = null;
  }

  async openFile() {
    await window.ipcRenderer.invoke("openFileDialog");
  }
}
