import { makeObservable, observable } from "mobx";
import { RootStore } from "./RootStore";

export class PopUpStore {
  root: RootStore;
  error: string | null = null;

  constructor(root: RootStore) {
    this.root = root;

    makeObservable(this, {
      error: observable,
    });
  }

  public showError(error: string): void {
    this.error = error;
  }
  clear(): void {
    this.error = null;
  }
}
