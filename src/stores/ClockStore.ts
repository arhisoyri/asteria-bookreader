import { RootStore } from "./RootStore";
import { action, makeObservable, observable } from "mobx";

export class ClockStore {
  //Need to redo it completely, so date can be private
  //Maybe, use annotations for MOBX
  root: RootStore;
  date: Date = new Date();
  private locale: string = "en";

  constructor(root: RootStore) {
    makeObservable(this, {
      date: observable,
      updateTime: action,
    });

    setInterval(() => {
      this.updateTime();
    }, 1000);
    this.root = root;
  }

  public clockAppearance(): string {
    //TODO Create Settings. It's not a state!
    //Create the clock format settings
    //24 hours and seconds
    const time = this.date.toLocaleTimeString(this.locale, {
      hour: "numeric",
      hour12: true,
      minute: "numeric",
      second: "numeric",
    });
    return time;
  }

  /// method to update date to current date-time (to be used from setInterval(clockTick))

  public updateTime(): void {
    this.date = new Date();
  }
}
