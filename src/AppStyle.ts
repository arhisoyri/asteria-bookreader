import { createStyles, Theme } from "@material-ui/core/styles";

export const styles = (theme: Theme) =>
  createStyles({
    wrapper: {
      display: "grid",
      height: "100vh",
      width: "100vw",
      gridTemplateRows: "64px auto ",
      gridTemplateColumns: "auto 4fr",
      gridTemplateAreas: `
        "sidebar header "
        "sidebar page "
        
        `,
    },
    header: {
      gridArea: "header",
      height: "64px",
      minHeight: "64px",
      //TODO Research @media!!!!
    },

    sidebar: {
      gridArea: "sidebar",
    },

    footer: {
      gridArea: "footer",
    },

    gridPage: {
      transition: theme.transitions.create(["margin", "width"], {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),

      minMax: "200px, 4fr",
      gridArea: "page",
      height: "100%",
      position: "relative",
      background: "linear-gradient(0deg, #191d35 30%, #0e1125 90%)",
    },
  });
